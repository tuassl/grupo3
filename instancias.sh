#!/bin/bash

#Importante: Para poder probar este ejercicio, deberá crear un script aparte
#que ejecute 10 instancias del proceso que Ud. desee y luego deberá obtener
#los id de proceso de ese programa y guardarlos en un archivo para alimentar
#la funcionalidad descripta.

function elimPro { 

        for i in $(seq 1 10); do

          $(yes &> /dev/null &)

        done
	
        ps ax | grep yes | awk -v 's=' '{ print s$1s }' > listaProcesos.csv

}

rm listaProcesos.csv
 
export -f elimPro
