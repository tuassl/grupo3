#!/bin/bash

#Reporta los 5 porcesos por mayor consumo de memoria
	echo
	echo "Los cinco procesos que mas memoria ram consumen son:"
	echo
	echo "USER %MEM  COMMAND"
	echo " |     |      | "
	echo " V     V      V " 
	ps aux --sort=%mem  	| tac 	| head -n 5 	| awk '{print $1,$4,$11}'


