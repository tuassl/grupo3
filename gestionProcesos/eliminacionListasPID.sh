#!/bin/bash

declare -i terminacionAmable
declare -i terminacionForzada
ruta=$(dirname $PWD)

#Extienda la funcionalidad del punto anterior, para eliminar una lista
#de procesos (PIDs) de manera de automatizar el trabajo de hacerlo uno por uno.

#Importante: Para poder probar este ejercicio, deber   crear un script aparte
#que ejecute 10 instancias del proceso que Ud. desee y luego deber   obtener
#los id de proceso de ese programa y guardarlos en un archivo para alimentar
#la funcionalidad descripta.
#Se utilizan las funciones es_root para autenticar que el usuario es root y la funcion  elimPro para usar la lista de procesos  generada

#Se reutiliza  parte del script eliminacionProcesoPID para reducir el trabajo
es_root
elimPro

echo "La eliminacion de los procesos por medio de una lista esta en proceso... "

terminacionAmable=0
terminacionForzada=0

bandera=true
cat listaProcesos.csv | while read $opt; do

if (ps aux $opt &> /dev/null); then

     if !(kill -15 $opt &> /dev/null); then

       terminacionAmable=$(($terminacionAmable+1))

    else

        if !(kill -9 $opt &> /dev/null); then

          terminacionForzada=$(($terminacionForzada+1))

        else

                echo
                echo "|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|"
                echo "El proceso no puede ser finalizado!"
                echo "|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|"
                echo

        fi

   fi 

 else

        echo
        echo "----------------------"
        echo " No existe el proceso "
        echo "----------------------"
        echo

 fi
bandera=false

#Hace un conteo de los procesos que terminan tanto de manera amable como forzada
echo "Se eliminaron $terminacionAmable procesos amable y $terminacionForzada procesos forzado "

done

rm $ruta/listaProcesos.csv
