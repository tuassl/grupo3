
#!/bin/bash

#El usuario ingresa el PID de un procesos para su eliminacion e
# intenta eliminarlo de forma pacifica.

declare -i y 
declare -i procesoPID

echo "Estos som los pprocesos disponibles para su eliminacion: "
echo

ps -e

echo
echo "Ingrese el PID del proceso que desea eliminar"
read procesoPID

#Con la opcion -o comm= dentro de ps nos devolvera el nombre del proceso
nombrePID=$(ps -p $procesoPID -o comm= > /dev/null 2>&1)

if [ $? -eq 0 ]; then
y=0
        echo
        echo "Eliminando proceso $procesoPID"
        echo "Nombre=> $nombrePID "
	echo
	kill -15 $procesoPID

	if [ $? -eq 0 ]; then
		echo
       		echo "+|||||||||||||||||||+"
        	echo "+Eliminacion exitosa+"
        	echo "+|||||||||||||||||||+"
		echo
	else
#En caso de que el proceso no pueda terminar de forma amable,
#se lo intenta matar con una orden mas fuerte

	while [$y -lt 2]; do
		kill -9 $procesoPID
		if [$? -eq 0];then
			y=4
		else

			y=$y+1
		fi
	done
		if [$y -lt 4]; then

		echo
		echo "|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|"
               	echo "El proceso no puede ser finalizado!"
                echo "|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|"
                echo

        else
        	echo
		echo "|+|+|+|+|+|+|+|+|+|+|+|+|+|+|"
                echo "Proceso eliminado a la fuerza"
		echo "|+|+|+|+|+|+|+|+|+|+|+|+|+|+|"
		echo
        fi

	fi

else
	echo
	echo "------------------------------------------------"
        echo "No existe un proceso con ese PID correspondiente"
	echo "------------------------------------------------"
	echo
fi
