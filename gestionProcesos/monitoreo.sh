#!/bin/bash

#Monitorear un proceso en especifico tomando por pantalla el PID que el usuario escoja.

echo "ingrese id del proceso (PID): " 
read procesoID

echo
echo "\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/"
echo "||||||||||||||||||||||||||||||||||||||||||||"
echo "============ MONITOREO EN PROCESO =========="
echo "||||||||||||||||||||||||||||||||||||||||||||"
echo "\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/"
echo

#Con top tira el proceso por pantalla unas diez veces con una iteracion de 3 segundos por cada monitoreo realizado
top -p $procesoID -b -n 10 -d 3
