#!/bin/bash

declare -i opcion

ruta=$PWD 
masConsumo=$ruta/gestionProcesos/masConsumoMemoria.sh
alerta=$ruta/gestionProcesos/umbral.sh
monitoreo=$ruta/gestionProcesos/monitoreo.sh
eliminacionPID=$ruta/gestionProcesos/eliminacionProcesoPID.sh
eliminacionPIDlista=$ruta/gestionProcesos/eliminacionListasPID.sh
#Es el menu de la  gestion de procesos

while true; do
	echo 
	echo "<><><><><><><>Gestion de procesos<><><><><><>"
	echo
	echo "1) Reporte procesos de mayor consumo de memoria"
	echo "2) Verificar cantidad de procesos"
	echo "3) Mostrar proceso PID"
	echo "4) Eliminar un proceso"
	echo "5) Eliminar una lista de procesos"
	echo "6) Salir"
	echo "7) AYUDA"
	echo
	echo "<><><><><><><><><><><><><><><><><><><><><><><>"
	echo
	echo "Selecione una opcion por favor: "
	echo
	read opcion

	case $opcion in
		1) bash $masConsumo;;
		2) bash $alerta;;
		3) bash $monitoreo;;
		4) bash $eliminacionPID;;
		5) bash $eliminacionPIDlista;;
		6) break;;
		7) clear
		   echo
		   echo "<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>"
		   echo
		   echo "OPCIONES: "
		   echo "La opcion 1 Muestra los primeros 5 procesos ordenados por consumo de memoria  "
		   echo "La opcion 2 Alerta si el numero de procesos es mayor al umbral provisto"
		   echo "La opcion 3 Muestra un proceso colocando el id del proceso correspodiente por 10 veces seguidas "
		   echo "La opcion 4 Elimina un proceso especifico dado su id de proceso "
		   echo "La opcion 5 Elimina una lista de procesos id "
		   echo
		   echo "<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>"
		   echo
		   ;; 
		*) echo "Opcion invalida, ingrese un numero del 1 al 7";;
	esac 
done

