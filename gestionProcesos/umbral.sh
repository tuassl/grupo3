#!/bin/bash

#Tira una advertencia si es que hay mas procesos que el umbral
# definido por el usuario

echo "Cual es la cantidad maxima de procesos que desea ingresar? "
read maxCantidadProc

maximoProc=$(ps aux | wc -l)
if [ "$maxCantidadProc" -lt "$maximoProc" ];then

	echo
	echo "El umbral es $maxCantidadProc y los procesos del sistema son $maximoProc " 
	echo
else
	echo
	echo "Advertencia, la cantidad de procesos supera el umbral provisto!!!"	
	echo
fi


