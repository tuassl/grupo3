#!/bin/bash

#Detecta si esta instalado el paquete iotop, si no lo esta lo instala y si esta lo ejecuta

declare -u resPaq2

dpkg -s iotop >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
	echo El paquete no se encuentra instalado
	echo ¿Desea instalar el paquete: y/n?
	read resPaq2
	if [[ $resPaq2 = 'Y' ]]; then
		echo Instalando el paquete iotop en su sistema...
		sudo apt install iotop
	else
		echo El paquete se encuentra correctamente instalado. Puede utilizarlo sin problemas.
	fi
else
	/usr/sbin/./iotop -o -b -n 3
fi
