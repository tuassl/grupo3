#!/bin/bash

declare -u resPaq

dpkg -s sysstat >/dev/null 2>&1
if [[ $? -ne 0 ]]; then	##Queremos saber si el paquete esta instalado
	echo El paquete no esta instalado en su sistema.
	echo ¿Desea instalar el paquete: y/n?
	read resPaq
	if [[ $resPaq = 'Y' ]]; then
		echo Instalando...
		sudo apt install sysstat
	else
		echo El paquete se encuentra correctamente instalado. Puede utilizarlo sin problemas.
	fi
else
	iostat -d -m 2 1
fi
