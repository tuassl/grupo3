#!/bin/bash

ruta=$PWD
reporteEspacio=$ruta/gestionDisco/reporteEspacio.sh
reporteRendimiento=$ruta/gestionDisco/reporteRendimiento.sh
reporteActividad=$ruta/gestionDisco/reporteActividad.sh

declare -i opcionDisco

while true; do

echo --------- OPCIONES DISCOS -----------
echo
echo 1- REPORTE DE INFORMACION DE ESPACIO 
echo 2- REPORTE DE RENDIMIENTO
echo 3- REPORTE DE ACTIVIDAD DE I/O
echo 4- SALIR
echo 5- AYUDA
echo
echo -------------------------------------
echo SELECCIONE UNA OPCION DEL 1-5
read opcionDisco

case $opcionDisco in

	1) bash $reporteEspacio;;
	2) bash $reporteRendimiento;;
	3) bash $reporteActividad;;
	4) break;;
    5) clear
    echo --------------------------- AYUDA -------------------------------
    echo 1- Muestra un reporte acerca del espacio en disco
    echo 2- Muestra un reporte de rendimiento del disco
    echo 3- Muestra un reporte acerca de la actividad de entrada y salida
    echo -----------------------------------------------------------------
;;
	*) echo Opcion incorrecta elegir una opcion del 1 al 5;;
esac
done
