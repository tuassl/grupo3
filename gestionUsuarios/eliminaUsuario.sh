#!/bin/bash

declare -l user

lista=$(cat listaUser.csv)  ##Es la lista de usuarios a crear
ruta=$PWD

#Mediante un archivo de nombres eliminar los usuarios correspondientes
#Solo root puede utilizar esta opcion

es_root

echo "Mediante un archivo .csv se eliminaran los usuarios de la lista"
echo
echo "Ingrese ruta completa del archivo *.csv para realizar el alta de usuarios"
	read -e usuariosBaja
	if [ -f $usuariosBaja ]; then
		echo " Fichero encontrado: "
	else
		echo " El fichero no existe"
		exit 1
	fi

lista=$(cat $usuariosBaja)

existe_user $lista

aviso_eliminacion

for user in $lista;do
       	deluser --remove-home $user > /dev/null 2>&1    ##Eliminamos el usuario pero sin mostrar la salida estandar
	echo -------------------------------------------------------------------
	echo Se asume toda la responsabilidad por la eliminacion de contenido    ##Politica de eliminacion de usuario
        echo -------------------------------------------------------------------
        echo --------------------------------
        echo "El usuario eliminado es:" $user
       	echo --------------------------------
done

