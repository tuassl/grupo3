#!/bin/bash

declare -i opcionUser
ruta=$PWD
altaUser=$ruta/gestionUsuarios/altaUsuarios.sh
cruceLlaves=$ruta/gestionUsuarios/cruceLlaves.sh
bajaUser=$ruta/gestionUsuarios/eliminaUsuario.sh
reporteDeUsuarioSistema=$ruta/gestionUsuarios/reporteUsuarioSist.sh
reporteUsuariosActivos=$ruta/gestionUsuarios/usuariosActivos.sh

clear

while true; do

	echo -------- OPCIONES USUARIOS ------------
	echo
	echo 1- CREAR USUARIOS
	echo 2- CRUCE DE LLAVES
	echo 3- ELIMINAR USUARIOS
	echo 4- REPORTE DE USUARIOS DE SISTEMAS
	echo 5- REPORTE DE USUARIOS ACTIVOS
	echo 6- SALIR
	echo 7- AYUDA
	echo 
	echo SELECCIONE UNA OPCION DEL 1-6

	read opcionUser

	case  $opcionUser in 
		1) bash $altaUser;;
		2) bash $cruceLlaves;;
		3) bash $bajaUser;;
		4) bash $reporteDeUsuarioSistema;;
		5) bash $reporteUsuariosActivos;;
		6) break;;
		7) clear
		   echo -----------------------------------------------------------------------------------
		   echo
		   echo "OPCIONES: "
		   echo "La opcion 1 Crea usuarios mediante un archivo .csv que usted proporciona "
		   echo "La opcion 2 Se realiza el cruce de llaves ssh "
		   echo "La opcion 3 Se elimaina usuarios mediante un archivo .csv que usted proporciona "
		   echo "La opcion 4 Se crea un reporte de los usuarios del sistema "
		   echo "La opcion 5 Se crea un reporte de los usuarios activos para login "
		   echo
		   echo ------------------------------------------------------------------------------------
		   ;;
		*) echo "Opcion invalida elija del 1-6!!";;
	esac
done
