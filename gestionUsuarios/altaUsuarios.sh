#!/bin/bash

##Se crearan usuarios mediante la entrada de un archivo .csv 
##Se analiza si es root ya que esta opcion solo la puede utilizar root
##Se verifica si el archivo existe
##En caso de existir se crean los usuarios
##Al final se le pide al usuario que cambie su password

declare -l user

ruta=$PWD

#Mediante un archivo de nombres crear los usuarios correspondientes

es_root

echo "Se utilizara un archivo .csv para crear los usuarios!!!"
echo 
echo "Ingrese ruta completa del archivo *.csv para realizar el alta de usuarios"
read -e usuariosAlta

if [ -f $usuariosAlta ]; then
	echo " Fichero encontrado: "
else
	echo " El fichero no existe"
	exit 1
fi

lista=$(cat $usuariosAlta)

existe_user $lista

for user in $lista; do
	useradd -m $user
	passwd $user
       	passwd -e $user  ##Obligamos al usuario a cambiar la contraceña en su primer inicio
        echo -------------------------------
        echo " El usuario creado es: " $user
        echo -------------------------------
done


