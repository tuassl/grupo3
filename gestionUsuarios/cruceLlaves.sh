#!/bin/bash

##Se realiza el cruce de llaves mediante un archivo .csv

ruta=$PWD
lisM=$(cat lisMaq.csv) ##Usuarios de las maquinas
pruebaLlave=/home/$(whoami)/.ssh/id_rsa.pub ##Comprobamos si la llave existe
dir=$(cat ipMaq.csv) ##Son las ip de las maquinas asignadas

echo ---------------------------------------------------------------------------------
echo Para realizar el cruce de claves necesita las password de las maquinas a conectar
echo ---------------------------------------------------------------------------------

if [[ -f $pruebaLlave ]]; then
	echo La llave existe, continuemos !!!
else
	echo Crear la llave
	ssh-keygen -t rsa
fi
for usuarios in $lisM;do
	echo ----------------------------
	echo El usuario es: $usuarios
	echo ----------------------------

	for ip in $dir;do 	##Con la entrada de IPs realizamos el cruze de claves
        	echo ---------------
		echo la IP es: $ip
		echo ---------------
                ssh-copy-id $ip
        done
done
