#!/bin/bash

#En este archivo colocaremos las funciones a utilizar

##Con esta funcion le preguntamos al usuario si es root o usuario normal

function es_root {

        if [ $(whoami) != "root" ]; then
                echo " Tienes que ser root para ejecutar esta opcion "
                echo " Ejecuta sudo su para ser root "
                exit 1
        else
                echo Sos root continuemos con la operacion!!!
        fi
}

##Esta funcion nos permite saber si un usuario existe o no en el sistema

function existe_user {

	for user in $1 ; do
		grep $user /etc/passwd
		if [[ $? -eq 0 ]]; then
			echo "El usuario existe"
			exit
		else
			echo "El usuario no existe, continuar"
		fi
	done
}

##Esta funcion es un aviso de eliminacion de cuenta de usuario

function aviso_eliminacion {

	echo "Se le avisa que su usuario será eliminado!!"
	echo "Realice un backups de sus archivos"
	echo "Tiene 30 segundos"

	sleep 30

	echo "Tiempo concluido!!!"

}

##Se realizan las exportaciones pendientes 

export -f es_root
export -f existe_user
export -f aviso_eliminacion
