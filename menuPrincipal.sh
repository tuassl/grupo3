#!/bin/bash

##Este es el menu principal que se le muestra al usuario para que eliga una accion
#Se a decidido usar el comando pwd para asignar la ruta de cada script 
#Utilizamos source para llamar a las funciones que estan en utilidades.sh

ruta=$PWD
usuarios=$ruta/gestionUsuarios/menu.sh
discos=$ruta/gestionDisco/menu.sh
procesos=$ruta/gestionProcesos/menu.sh
memoria=$ruta/gestionRam/menuRam.sh
texto=$ruta/gestionTexto/menu.sh

source utilidades.sh
source instancias.sh
declare -u resp
declare -i opcionUsuario

function menuPrincipal {

        clear
	echo -----------------------------------------------
	echo "*********** MENU DE OPCIONES ******************"
	echo -----------------------------------------------

	echo 1- GESTION DE USUARIOS

	echo 2- GESTION DE DISCOS

	echo 3- GESTION DE PROCESOS

	echo 4- GESTION DE MEMORIA

	echo 5- GESTION DE TEXTO

	echo 6- AYUDA

	echo 7- SALIR

	echo Seleccione una opcion disponible
	read opcionUsuario

case $opcionUsuario in
	1)bash $usuarios;;
	2)bash $discos;;
	3)bash $procesos;;
	4)bash $memoria;;
	5)bash $texto;;
	6) clear
	   echo "--------------------------------------------------------------"
	   echo "La opcion 1 nos permite gestionar los usuarios del sistema "
	   echo "La opcion 2 nos permite gestionar los discos "
   	   echo "La opcion 3 nos permite la gestion de procesos "	   
	   echo "La opcion 4 nos permite la gestion de memoria Ram "
	   echo "La opcion 5 nos permite la gestion de texto "
	   echo "--------------------------------------------------------------"
	;;
	7)break;;
	*)echo "Error, ingrese un numero del 1-6!!!"
esac

}

while true;do

	menuPrincipal
	echo Si desea hacer otra consulta teclee "s" sino "n":
	read resp

	if [ $resp = 'S' ]; then
		echo Continuemos!!!
	else
		echo Gracias por su consulta!!!
		break
	fi
done
echo ------------------
echo FIN DEL PROYECTO
echo ------------------


