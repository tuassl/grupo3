# Grupo 3 
---------
### Alumnos:
- Geraldin Nasabun
- Rodrigo Benegas
- Emilio Arredondo

#### Branch evaluada: master

#Etapas del proyecto

###Menu: 95%
-----
1) Al hacer clic en la opción 7 (salir) da error: menuPrincipal.sh: line 58: break: only meaningful in a `for', `while', or `until' loop

###Usuarios: 80%
--------
1) No queda claro que los submódulos están recibiendo un listado hardcodeado, por lo que dificulta un poco el uso. (no saca puntos)
2) Debería haber listado los usuario del S.O no de todo el sistema, con lo cual no debía filtrar aquellos usuarios que sean del sistema operativo cuyo id es menor a 1000
3) Los usuarios activos no alcanza con chequear /bin/bash porque podrían tener otro shell, además de eso debería haber verificado que el id esté entre 1000 y 2000.

###Discos: 50%
-------
1) El reporte de información de espacio en disco debería funcionar para cada servidor, solo lo hizo en forma local.
2) El reporte de actividad tiene el mismo problema que en (1) sólo lo resuelve local.
3) Al momento de instalar iotop (si es que no está instalado) realiza la instalación pero no genera el reporte y debería. (evitando ejecutar 2 veces)

###PROCESOS: 100%
---------
OK!
1) Podría haber reusado el código de eliminación que realizó previamente con cada pid, entonces cuando ingresa un listado simplemente era realizar el llamado
al del caso base.

###MEMORIA: 100%
--------
OK!
1) Olvidó crear el archivo temporal con mktemp

###TEXTO: 90%
------
Falta el control de permisos para modificar el /etc/resolv.conf

#Devolución general:
-------------------
El proyecto está bastante bien logrado. La organización y documentación está completa (aunque podría mejorarse).
 - El punto más flojo es no haber realizado la resolución remota de los discos en el resto de las vm con ssh ya que solo lo realiza con la vm local.
 - Podría haber reusado código en la parte de procesos y no repetir código.

Puntuación general del trabajo <b>8 pts.</b> Buen trabajo!


