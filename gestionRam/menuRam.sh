#!/bin/bash

##Vemos las opciones de RAM

ruta=$PWD
reporte=$ruta/gestionRam/reporteRam.sh
memory_alert=$ruta/gestionRam/alerta.sh

clear

while true; do
	echo -------- OPCIONES DE RAM ------------
	echo
	echo 1 - REPORTE DE USO DE RAM
	echo 2 - EJECUTAR memory-alert
	echo 3 - SALIR
	echo 4 - AYUDA
	echo
	echo -------------------------------------

	read op

	case $op in
		1) bash $reporte;;
		2) bash $memory_alert;;
		3)break;;
		4) clear
		   echo "-------------------------------------------------------------------------------------------------"
		   echo 
		   echo "La opcion 1 Nos brinda un reporte del uso de la memoria ram en el sistema "
		   echo "La opcion 2 Se ejecuta un programa que nos avisa si el total de la memoria ram es muy elevada "
		   echo
		   echo "-------------------------------------------------------------------------------------------------"
		;;
		*) echo Opcion INVALIDA elija del 1 al 3 !!!;;
	esac
done
