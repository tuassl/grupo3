#!/bin/bash

##Este programa analiza si el total de ram es mayor a el numero umbral asignado, 
##enviara el informe a un archivo temporal, que 
##luego de mostrarse por pantalla se borra.

declare -u resp

##Se realiza la instalacion del programa para recibir correos en el sistema

dpkg -s mailutils > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "El programa no se encuentra instalado!!"
	echo "Necesita tener el programa instalado en su equipo"
	echo "Quiere instalar el programa para recibir los meil ? S/n?"
	read resp
	if [[ $resp == "S" ]]; then
		sudo apt-get install mailutils
	else 
		echo "No se instalara el programa"
		exit
	fi
else
	echo "El programa se encuentra instalado"
	echo "Continuemos con la ejecucion"
fi

ramusage=$(free -h | awk '/Mem/{printf("RAM Uso: %.2f\n"), $3/$2*100}'| awk '{print $3}')

echo "Ingrese un valor"
read valor

if [ "$ramusage" > $valor ]; then
SUBJECT="ATENCION: El uso de memoria es alto en $(hostname) at $(date)"
MESSAGE=$(mktemp)
TO=$(echo $HOSTNAME"@mail.com")

	echo "La cantidad de memoria en uso es: $ramusage%" >> $MESSAGE
	echo "" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "Consumo de memoria con información de TOP" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "$(top -b -o +%MEM | head -n 20)" >> $MESSAGE
	echo "" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "Consumo de memoria con información de PS" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "$(ps -eo pid,ppid,%mem,%cpu,cmd --sort=-%mem | head)" >> $MESSAGE
	
	echo "Cuerpo:"| cat $MESSAGE | mail -s "Asunto : $SUBJECT" $TO

cat $MESSAGE
rm ${MESSAGE}

fi
