ruta=$PWD
verificarHosts=$ruta/gestionTexto/verificarHosts.sh
listarDNS=$ruta/gestionTexto/listarDNS.sh
declare -i opcionTexto

while true; do

	echo -------- OPCIONES TEXTO ------------
	echo
	echo 1- VERIFICACION DE ARCHIVO HOST
	echo 2- LISTAR LOS DNS
	echo 3- SALIR
    echo 4- AYUDA
	echo
	echo SELECCIONE UNA OPCION DEL 1-4
	read opcionTexto

	case $opcionTexto in

		1) bash $verificarHosts;;
		2) bash $listarDNS;;
		3) break;;
        4) echo ------------------------------------------- AYUDA ----------------------------------------------------
           echo 1- Busca que en el archivo de /etc/hosts se encuentre correctamente configurada la IP de host local.
           	Si no esta correctamente configurada dirijase al archivo /etc/hosts y agregue la entrada manualmente.
           echo 2- Lista los DNS que utiliza y te permite agregar mas direcciones en el archivo /etc/resolv.conf.
           echo ------------------------------------------------------------------------------------------------------
;;
		*) echo "Opcion incorrecta elegir una opcion del 1 al 4";;
	esac
done

