#!/bin/bash

#ruta=$PWD
host=$ruta/etc/hosts #El archivo donde se encuentran agregados los hosts con su correspondiente IP
objetivo=$HOSTNAME #El usuario/host logueado

echo Buscando si la configuracion es correcta en archivo correspondiente...

egrep -i -q *$objetivo* $host #Busca y encuentra lineas que coinciden con la palabra y en la ruta dada ignorando mayusculas y sin mostrar por pantalla.
if [ $? -ne 0 ];then
	echo AVISO: El host local no se encuentra correctamente configurado en el archivo /etc/hosts
else
	echo Configuracion correcta
fi
